package com.markacz.lineAppend;

public class LineAppendComma extends LineAppend {

    @Override
    protected CharSequence getAppendString() {
        return ",";
    }

}
