package com.markacz.lineAppend;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.editor.*;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

abstract public class LineAppend extends AnAction {

    abstract protected CharSequence getAppendString();

    @Override
    public void actionPerformed(@NotNull AnActionEvent event) {
        Project project = event.getRequiredData(CommonDataKeys.PROJECT);
        Editor editor = event.getRequiredData(CommonDataKeys.EDITOR);

        Document document = editor.getDocument();
        Caret caret = editor.getCaretModel().getPrimaryCaret();

        int line = document.getLineNumber(caret.getOffset());

        Logger.getInstance("markacz").info(String.format("line=%d", line));

        WriteCommandAction.runWriteCommandAction(project, () ->
                document.insertString(document.getLineEndOffset(line), getAppendString())
        );
    }

}
