package com.markacz.lineAppend;

public class LineAppendSemicolon extends LineAppend {

    @Override
    protected CharSequence getAppendString() {
        return ";";
    }

}
